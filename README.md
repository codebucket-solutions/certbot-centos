# Certbot Installation on  Centos/Rhel (SDC) (Alongside Nginx)



## Installation Steps
1. Check if python3 is installed. (python3 -V) check version (python3.6 support will be dropped from certbot)
If not install python  
```
    sudo dnf install python3 
```

2. Install / Upgrade pip  
```
    pip3 install --upgrade pip
```
3. Install certbot  
```
    pip3 install certbot
```
4. Move certbot from its installed location to a location available on $PATH
```  
    ln -s /usr/local/bin/certbot /usr/bin/certbot
```
5. Upgrade pyopenssl  
```
    pip3 install pyopenssl --upgrade
```
6. Install certbot nginx plugin  
```
    pip3 install certbot-nginx
```
7. Run certbot  
```
    certbot --nginx
```

## GOOGLE IF YOU RUN INTO ANY ERRORS. 
